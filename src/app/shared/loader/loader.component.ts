import { Component, OnInit } from '@angular/core';
import { LoaderService } from '../../service/loader.service';

@Component({
  selector: 'app-loader',
  templateUrl: './loader.component.html',
  styleUrls: ['./loader.component.css']
})
export class LoaderComponent implements OnInit {

  loading: boolean;
  percent: any;
  progress: any;
  constructor(private loaderService: LoaderService) {
    this.loaderService.isLoading.subscribe((v) => {
      if(!v){
        this.growProgress(100, () =>{
          this.loading = v;
        });
      }else{
        this.loading = v;
        this.percent = {
          width: "5%"
        };
        this.progress = 5;
        this.growProgress(90,);
      }
    });
  }

  ngOnInit() {
  }

  growProgress(max, cb = null){
    var myInterval = setTimeout(() => {
      this.progress++;
      this.percent = {
        width: `${this.progress}%`
      }
      if (this.progress > max ){ 
        clearInterval(myInterval)
        if(cb){
          cb()
        }
      }else{
        this.growProgress(max, cb)
      }
    },50);
  }


}
