import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-input',
  templateUrl: './input.component.html',
  styleUrls: ['./input.component.css']
})
export class InputComponent implements OnInit {

  @Input()
  val: any;

  @Input()
  type: any;

  @Input()
  name: any;

  @Input()
  placeholder: any;

  @Input()
  label: any;

  @Input()
  helper: any;

  constructor() { }

  ngOnInit() {
  }

}
