import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
// import { AuthService } from './auth-service.service';

@Injectable({
  providedIn: 'root'
})
export class CoreService {

  public header: any;
  public user: any;

  constructor(private http: HttpClient) { 
    // this.user = this.authService.getUser();
    this.user = null;
    if(this.user){
      this.header = new HttpHeaders ({
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${this.user.accessToken}`
      });
    }else{
      this.header = new HttpHeaders ({
        'Content-Type': 'application/json'
      });
    }
  }

  get(url){
    return new Promise((resolve, reject) => {
      this.http.get(url, { headers: this.header , 
        reportProgress: true
      }).subscribe((result) => {
        resolve(result);
      }, async (error) => {
        try{
          let msg = error.error.message;
          let code = error.status;
          reject({
            code,
            msg
          });
        }catch(e){
          reject(e)
        }
      })
      
    })
  }

  post(url, data){
    return new Promise((resolve, reject) => {
      this.http.post(url, data, { headers: this.header }).subscribe((result) => {
        resolve(result);
      }, async (error) => {
        try{
          let msg = error.error.message;
          let code = error.status;
          let inside = error.error.error
          reject({
            code,
            msg,
            inside
          });
        }catch(e){
          reject(e)
        }
      })
    })
  }

  patch(url, data){
    return new Promise((resolve, reject) => {
      this.http.patch(url, data, { headers: new HttpHeaders ({
        'Content-Type': 'application/json',
        'authorization': `Bearer ${this.user.accessToken}`
      }) }).subscribe((result) => {
        resolve(result);
      }, async (error) => {
        try{
          let msg = error.error.message;
          let code = error.status;
          let inside = error.error.error
          reject({
            code,
            msg,
            inside
          });
        }catch(e){
          reject(e)
        }
      }) 
    })
  }

  getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for(var i = 0; i < ca.length; i++) {
      var c = ca[i];
      while (c.charAt(0) == ' ') {
        c = c.substring(1);
      }
      if (c.indexOf(name) == 0) {
        return c.substring(name.length, c.length);
      }
    }
    return "";
  }
  
}
