import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { HTTP_INTERCEPTORS, HttpClient, HttpClientModule } from '@angular/common/http';

import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';

import { ChartsModule } from 'ng2-charts';

import { AppComponent } from './app.component';
import { AppRoutes } from './app.routing';

import { NavbarComponent } from './shared/navbar/navbar.component';
import { LoginComponent } from './view/login/login.component';
import { ViewComponent } from './view/view.component';
import { AdsNavbarComponent } from './shared/ads-navbar/ads-navbar.component';
import { AdsSearchComponent } from './shared/ads-search/ads-search.component';
import { AdsTopComponent } from './shared/ads-top/ads-top.component';
import { RegisterComponent } from './view/register/register.component';
import { LandingPageComponent } from './view/landing-page/landing-page.component';
import { LoaderComponent } from './shared/loader/loader.component';

import { LoaderService } from './service/loader.service';
import { LoaderInterceptor } from './loader.interceptor';
import { FooterComponent } from './shared/footer/footer.component';
import { InputComponent } from './shared/input/input.component';
import { RewardComponent } from './view/reward/reward.component';
import { TransactionComponent } from './view/transaction/transaction.component';
import { TranscationFormComponent } from './view/transaction/transcation-form/transcation-form.component';
import { TransactionCardComponent } from './shared/transaction-card/transaction-card.component';
import { RightMenuComponent } from './shared/right-menu/right-menu.component';
@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    LoginComponent,
    ViewComponent,
    AdsNavbarComponent,
    AdsSearchComponent,
    AdsTopComponent,
    RegisterComponent,
    LandingPageComponent,
    LoaderComponent,
    FooterComponent,
    InputComponent,
    RewardComponent,
    TransactionComponent,
    TranscationFormComponent,
    TransactionCardComponent,
    RightMenuComponent
  ],
  imports: [
    ChartsModule,
    BrowserModule,
    FormsModule,
    HttpClientModule,
    RouterModule.forRoot(AppRoutes),
  ],
  providers: [
    LoaderService,
    { provide: HTTP_INTERCEPTORS, useClass: LoaderInterceptor, multi: true }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
