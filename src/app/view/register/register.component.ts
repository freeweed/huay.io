import { Component, OnInit } from '@angular/core';

declare var $:any;

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  public showText;
  public showList = ["Personal Information","Company Information","Connect Your Digital Marketing Account"]
  constructor() { 
    this.showText = "Personal Information";
  }

  ngOnInit() {

  }

  changeTab(i){
    this.showText = this.showList[i-1];
    $(".cus-tabs").hide()
    $(`#tabs-${i}`).show()
  }

}
