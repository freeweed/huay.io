import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TranscationFormComponent } from './transcation-form.component';

describe('TranscationFormComponent', () => {
  let component: TranscationFormComponent;
  let fixture: ComponentFixture<TranscationFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TranscationFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TranscationFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
