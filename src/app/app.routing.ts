
import { Routes } from '@angular/router';

import { LandingPageComponent } from './view/landing-page/landing-page.component';
import { RewardComponent } from './view/reward/reward.component';
import { LoginComponent } from './view/login/login.component';
import { RegisterComponent } from './view/register/register.component';
import { ViewComponent } from './view/view.component';
import { TransactionComponent } from './view/transaction/transaction.component';

export const AppRoutes: Routes = [{
    path: '',
    redirectTo: 'home',
    pathMatch: 'full',
},{
    path: '',
    component: ViewComponent,
    children: [{
        path: 'home',
        component: LandingPageComponent,  
    },{
        path: 'reward',
        component: RewardComponent,
        
    },{
        path: 'transaction',
        component: TransactionComponent,
        
    }]
},{
    path: 'login',
    component: LoginComponent,
},{
    path: 'register',
    component: RegisterComponent,
}];
